package com.itcast.ssm.service;

import com.itcast.ssm.pojo.User;

import java.util.List;

public interface UserService {

    /**
     * 查询所有
     * @return
     */
    List<User> findAll();

    /**
     * 新增
     * @param user
     * @return
     */
    User add(User user);

    /**
     * 删除
     * @param uuid
     * @return
     */
    User delete(Integer uuid);

    /**
     * 查询单用户的信息
     * @param uuid
     * @return
     */
    User findOne(Integer uuid);

    /**
     * 修改用户
     * @param user
     * @return
     */
    User update(User user);
}
