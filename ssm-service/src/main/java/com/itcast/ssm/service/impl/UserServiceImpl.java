package com.itcast.ssm.service.impl;

import com.itcast.ssm.mapper.UserMapper;
import com.itcast.ssm.pojo.User;
import com.itcast.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional  //事物
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    /**
     * 查询所有
     * @return
     */
    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    /**
     * 新增
     * @param user
     * @return
     */
    @Override
    public User add(User user) {
        return userMapper.add(user);
    }

    /**
     * 删除
     * @param uuid
     * @return
     */
    @Override
    public User delete(Integer uuid) {
        return userMapper.delete(uuid);
    }

    /**
     * 查询单用户的信息
     *
     * @param uuid
     * @return
     */
    @Override
    public User findOne(Integer uuid) {
        return userMapper.findOne(uuid);
    }

    /**
     * 修改用户
     *
     * @param user
     * @return
     */
    @Override
    public User update(User user) {
        return userMapper.update(user);
    }
}
