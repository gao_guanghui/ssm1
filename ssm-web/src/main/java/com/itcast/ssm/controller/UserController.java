package com.itcast.ssm.controller;

import com.itcast.ssm.enums.StatusCode.ResponseResult;
import com.itcast.ssm.pojo.User;
import com.itcast.ssm.service.UserService;
import com.sun.org.apache.bcel.internal.classfile.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.transform.Result;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 查询所有
     * @return
     */
    @GetMapping
    public List<User> findAll(){
        return userService.findAll();
    }

    /**
     * 新增
     * @param user
     * @return
     */
    @PostMapping
    public ResponseResult<User> add(@RequestBody User user){
        return ResponseResult.okResult(userService.add(user));
    }

    /**
     * 删除
     * @param uuid
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseResult<User> delete(@PathVariable("id") Integer uuid){
        return ResponseResult.okResult(userService.delete(uuid));
    }

    /**
     * 查询单个用户信息
     * @param uuid
     * @return
     */
    @GetMapping("/{id}")
    public ResponseResult<User> findOne(@PathVariable("id") Integer uuid){
        return ResponseResult.okResult(userService.findOne(uuid));
    }

    /**
     * 修改用户
     * @param user
     * @return
     */
    @PutMapping
    public ResponseResult<User> update(@RequestBody User user){
        return ResponseResult.okResult(userService.update(user));
    }

}
