package com.itcast.ssm.mapper;

import com.itcast.ssm.pojo.User;

import java.util.List;

public interface UserMapper {

    /**
     * 查询所有
     * @return
     */
    List<User> findAll();


    /**
     * 新增
     * @param user
     * @return
     */
    User add(User user);

    /**
     * 删除
     * @return
     */
    User delete(Integer uuid);

    /**
     * 查询单个用户的信息
     * @param uuid
     * @return
     */
    User findOne(Integer uuid);

    /**
     * 修改
     * @param user
     * @return
     */
    User update(User user);
}
